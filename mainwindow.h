#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>

#include <QMainWindow>
#include <QGeoPositionInfoSource>
#include <QNmeaSatelliteInfoSource>

#include "satinfo.h"
#include "positioncalculator.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked(bool checked);
    void satellitesInViewUpdated(const QList<QGeoSatelliteInfo> &satellites);
    void satellitesInUseUpdated(const QList<QGeoSatelliteInfo> &satellites);

    void on_pushButton_clear_clicked();
    void positionUpdated(void);


private:
    Ui::MainWindow *ui;

    PositionCalculator calculator;

    SatInfo* satellite_source = nullptr;

    QGeoCoordinate homeposition;
    std::vector<QGeoSatelliteInfo> used_satellites;  // used satellites
    QString used_satellites_str;  // used satellites
};
#endif // MAINWINDOW_H
