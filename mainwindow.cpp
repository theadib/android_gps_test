#include <QTimer>

#include <tuple>
#include <algorithm>

#include <QScrollBar>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->textEdit_satinfo->append("detect: geo sources");
    for(auto i: QGeoPositionInfoSource::availableSources()) {
        ui->textEdit_satinfo->append(i);
    }

    connect(&calculator, &PositionCalculator::updated, this, &MainWindow::positionUpdated);
    satellite_source = static_cast<SatInfo*>(QGeoSatelliteInfoSource::createDefaultSource(this));
    if(satellite_source) {
        ui->textEdit_satinfo->append("satsource: "+ satellite_source->sourceName());
        connect(satellite_source, &QGeoSatelliteInfoSource::satellitesInViewUpdated, this, &MainWindow::satellitesInViewUpdated);
        connect(satellite_source, &QGeoSatelliteInfoSource::satellitesInUseUpdated, this, &MainWindow::satellitesInUseUpdated);
    } else {
        ui->textEdit_satinfo->append("no satinfo source");
    }

    ui->textEdit_statistics->append(calculator.name());
    auto *timer = new QTimer(this);
    timer->setSingleShot(false);
    timer->start(100);

    connect(timer, &QTimer::timeout, this, [=]() { auto v = ui->progressBar->value(); if(v) { ui->progressBar->setValue(v-1);}; } );
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked(bool checked)
{

    if(checked) {
        calculator.start(ui->spinBox->value());
    } else {
        calculator.stop();
    }


    ui->checkBox_usesatinfo->setEnabled(!checked);
    if(satellite_source) {
        if(checked && ui->checkBox_usesatinfo->isChecked()) {
            satellite_source->setUpdateInterval(5000);
            satellite_source->startUpdates();
        } else {
            satellite_source->stopUpdates();
        }
    }
}

void MainWindow::satellitesInViewUpdated(const QList<QGeoSatelliteInfo> &satellites)
{
    ui->textEdit_satinfo->clear();

    std::vector<QGeoSatelliteInfo> sats;
    for(const auto &s: satellites) {
        sats.push_back(s);
    }

    std::sort(sats.begin(), sats.end(), [](const auto &a, const auto &b) {
            return (a.satelliteIdentifier() != b.satelliteIdentifier()) ? a.satelliteIdentifier() < b.satelliteIdentifier() : a.satelliteSystem() < b.satelliteSystem();});
    int gpssats = std::count_if(sats.begin(), sats.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::GPS; });
    int glonasssats = std::count_if(sats.begin(), sats.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::GLONASS; });
    int beidousats = std::count_if(sats.begin(), sats.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::BEIDOU; });
    int gallileosats = std::count_if(sats.begin(), sats.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::GALILEO; });
    ui->textEdit_satinfo->append(tr("view total: %1, US:%2, EU:%3, RU:%4, CN:%5").arg(sats.size()).arg(gpssats).arg(gallileosats).arg(glonasssats).arg(beidousats));
    ui->textEdit_satinfo->append(used_satellites_str);
    for(const auto &s: sats) {
        int use_satinfo = 1;
        if(!ui->pushButton_showall->isChecked()) {
            // check if is used
            use_satinfo = std::count_if(used_satellites.begin(), used_satellites.end(), [s](const auto &u) { return ((s.satelliteIdentifier() == u.satelliteIdentifier() ) && (s.satelliteSystem() == u.satelliteSystem())); });
        }
        if(use_satinfo) {
            QString str = tr("ID:%1-%2 Signal:%3").arg(s.satelliteIdentifier()).arg(s.satelliteSystem()).arg(s.signalStrength());
            if(s.hasAttribute(QGeoSatelliteInfo::Elevation) && s.hasAttribute(QGeoSatelliteInfo::Azimuth)) {
                str+= tr(" elev:%1, azimuth:%2").arg(s.attribute(QGeoSatelliteInfo::Elevation)).arg(s.attribute(QGeoSatelliteInfo::Azimuth));
            }
            ui->textEdit_satinfo->append(str);
        }
    }
    QScrollBar *vScrollBar = ui->textEdit_satinfo->verticalScrollBar();
    vScrollBar->triggerAction(QScrollBar::SliderToMinimum);
}

void MainWindow::satellitesInUseUpdated(const QList<QGeoSatelliteInfo> &satellites)
{
    used_satellites.clear();
    for(const auto &s: satellites) {
        used_satellites.push_back(s);
    }

    int gpssats = std::count_if(used_satellites.begin(), used_satellites.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::GPS; });
    int glonasssats = std::count_if(used_satellites.begin(), used_satellites.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::GLONASS; });
    int beidousats = std::count_if(used_satellites.begin(), used_satellites.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::BEIDOU; });
    int gallileosats = std::count_if(used_satellites.begin(), used_satellites.end(), [](const auto &a) { return a.satelliteSystem() == QGeoSatelliteInfo::GALILEO; });
    used_satellites_str = tr("used total: %1, US:%2, EU:%3, RU:%4, CN:%5").arg(used_satellites.size()).arg(gpssats).arg(gallileosats).arg(glonasssats).arg(beidousats);
}


void MainWindow::on_pushButton_clear_clicked()
{
    calculator.clear();
}

void MainWindow::positionUpdated()
{

}

