#ifndef SATINFO_H
#define SATINFO_H

#include <QGeoSatelliteInfoSource>
#include <QObject>

class SatInfo : public QGeoSatelliteInfoSource
{
    Q_OBJECT
public:
    explicit SatInfo(QObject *parent = nullptr);
};

#endif // SATINFO_H
