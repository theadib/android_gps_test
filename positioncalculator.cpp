#include "positioncalculator.h"

PositionCalculator::PositionCalculator(QObject *parent)
    : QObject{parent}
{
    geo_source = QGeoPositionInfoSource::createDefaultSource(this);

    if(geo_source) {
        connect(geo_source, &QGeoPositionInfoSource::positionUpdated, this, &PositionCalculator::positionUpdated);
    }

}

QString PositionCalculator::name()
{
    if(geo_source) {
        return geo_source->sourceName();
    }
    return "";
}

/// start capturing in the given interval of seconds
void PositionCalculator::start(double interval)
{
    geo_source->setUpdateInterval(interval * 1000.);
    geo_source->startUpdates();
    positions.clear();
}

void PositionCalculator::stop()
{
    geo_source->stopUpdates();
}

void PositionCalculator::clear()
{
    positions.clear();
    update_statistics();
}

void PositionCalculator::positionUpdated(const QGeoPositionInfo &update)
{
    qDebug() << update;
    auto coordinate = update.coordinate();
    auto datetime = update.timestamp();

    // only collect valid updates
    if(coordinate.isValid() && datetime.isValid()) {
        positions.push_back(update);
        update_statistics();
    }


    if(update.hasAttribute(QGeoPositionInfo::HorizontalAccuracy)) {
        double accuracy = update.attribute(QGeoPositionInfo::HorizontalAccuracy);
    }

}

void PositionCalculator::update_statistics()
{
    if(positions.size()>=2) {

        qint64 diff = positions[0].timestamp().msecsTo(positions[positions.size()-1].timestamp());
        // ui->textEdit_statistics->append(tr("time consumed: %1 seconds").arg(diff / 1000));

        std::vector<QGeoCoordinate> coordinates(positions.size());

        // extract coordinates
        std::transform(positions.begin(), positions.end(), coordinates.begin(),
                       [](const auto &p){ return p.coordinate(); } );
        auto average_position =  std::accumulate(coordinates.begin(), coordinates.end(), std::tuple<double, double>{0.0, 0.0},
                                                 [](std::tuple<double, double> a, const auto &b)
                                                     {
                                                         return std::tuple<double, double> {std::get<0>(a)+b.latitude(),
                                                                std::get<1>(a) + b.longitude() };
                                                     });
        meanposition = QGeoCoordinate(std::get<0>(average_position)/coordinates.size(),
                                      std::get<1>(average_position)/coordinates.size());

        // print average and movement
        // ui->textEdit_statistics->append(tr("average position: %1°, %2°").arg(meanposition.latitude(),8,'f',7).arg(meanposition.longitude(),8,'f',7));
        // ui->textEdit_statistics->append(tr("distance last position to average: %1 m").arg(meanposition.distanceTo(coordinates[coordinates.size()-1]), 3, 'f', 2));
        // ui->textEdit_statistics->append(tr("distance last position to previous: %1 m").arg(coordinates[coordinates.size()-1].distanceTo(coordinates[coordinates.size()-2]), 3, 'f', 2));

        // differences to average
        std::vector<double> errors(coordinates.size());
        std::transform(coordinates.begin(), coordinates.end(), errors.begin(),
                       [this](const auto &c){ return c.distanceTo(meanposition); } );
        std::sort(errors.begin(), errors.end());


        double average_error = std::accumulate(errors.begin(), errors.end(), 0.0) / errors.size();
        // ui->textEdit_statistics->append(tr("average error: %1 m, maximum error: %2 m").arg(average_error,3,'f',2).arg(errors[errors.size()-1],3,'f',2));
        if(coordinates.size() >= 10) {
            double error_50 = errors[floor(errors.size()*0.5)-1];
            double error_80 = errors[floor(errors.size()*0.8)-1];
            // ui->textEdit_statistics->append(tr("radius 80%: %1 m, radius 50%: %2 m").arg(error_80,3,'f',2).arg(error_50,3,'f',2));
        }
    }
}


