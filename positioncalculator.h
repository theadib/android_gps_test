#ifndef POSITIONCALCULATOR_H
#define POSITIONCALCULATOR_H

#include <QObject>
#include <QGeoPositionInfoSource>
#include <vector>

class PositionCalculator : public QObject
{
Q_OBJECT

public:
    explicit PositionCalculator(QObject *parent = nullptr);
    QString name(void);
public slots:
    void start(double interval);
    void stop(void);
    void clear(void);

private slots:
    void positionUpdated(const QGeoPositionInfo &update);

signals:
    void updated(void);


signals:

private:

    void update_statistics(void);

    QGeoPositionInfoSource *geo_source = nullptr;
    QGeoCoordinate meanposition;

    std::vector<QGeoPositionInfo> positions;

};

#endif // POSITIONCALCULATOR_H
